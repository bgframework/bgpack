import csv
import gzip

import bglogs

from bgpack.implementation.parser import optimize, print_optimization
from bgpack.parse import AbstractParserGrouped, ParserError


class ParserDefault(AbstractParserGrouped):

    SCORE_ALT = {'T': 'ACG', 'A': 'CGT', 'C': 'AGT', 'G': 'ACT'}

    @staticmethod
    def parse(line):
        pos = int(line['Pos'])
        chrom = line.get('Chrom', line['#Chrom'])
        ref = line['Ref']
        alt = line['Alt']
        score = float(line['PHRED'])
        return chrom, pos, ref, alt, score

    @staticmethod
    def read(reader, check=False):
        v1 = ParserDefault.parse(next(reader))
        # Skip M and R labeled nucleotides
        while v1[2] in ['M', 'R']:
            v1 = ParserDefault.parse(next(reader))
        v2 = ParserDefault.parse(next(reader))
        v3 = ParserDefault.parse(next(reader))

        if check and not (v1[0] == v2[0] == v3[0] and v1[1] == v2[1] == v3[1] and v1[2] == v2[2] == v3[2]):
            raise ParserError('These lines are not consistent {}, {}, {}'.format(v1, v2, v3))
        position = v1[1]
        chromosome = v1[0]
        ref = v1[2]

        alts = ParserDefault.SCORE_ALT[ref]
        scores = [None] * 3
        for i, a in enumerate(alts):
            for v in [v1, v2, v3]:
                if v[3] == a:
                    scores[i] = v[4]
                    break

        return chromosome, (position, ref) + tuple(scores)

    def __init__(self, file):

        bglogs.debug('Reading CADD scores from {}', file)

        self.file = file

    def get_parsers(self):
        """

        Yield:
            tuple: key and the associated parser for that key

        """
        current_key = None
        first_val = None

        keep_going = True

        with gzip.open(self.file, 'rt') as fd:
            fd.readline()
            reader = iter(csv.DictReader(fd, delimiter='\t'))

            while keep_going:

                # Read first key and first value skipping comments
                if current_key is None:
                    current_key, first_val = ParserDefault.read(reader)

                # Create a parser
                def parser(check=True):
                    nonlocal current_key, first_val, keep_going

                    yield first_val

                    while True:
                        try:
                            this_key, data = ParserDefault.read(reader, check)
                        except StopIteration:
                            keep_going = False
                            break
                        except KeyError:
                            bglogs.debug('Skipping {}-{}'.format(this_key, data))
                            continue  # skip lines with unkown ref
                        if this_key != current_key:
                            current_key = this_key
                            first_val = data
                            break
                        else:
                            yield data

                bglogs.debug('Parser for key: {}', current_key)

                yield current_key, parser


if __name__ == '__main__':
    bglogs.configure('bgpack', debug=True)
    cadd_path = '/workspace/datasets/bgdata/local/genomicscores/cadd/1.0-20160829/whole_genome_SNVs.tsv.gz'

    # Optimization
    parser_ = ParserDefault(cadd_path)
    print_optimization(optimize(parser_))

    # Construction
    # out = 'cadd1.0'
    # os.makedirs(out)
    # parser_ = ParserOpt(cadd_path)
    # write('{}/cadd'.format(out), parser_, allow_multifile=True, optimize_for='size', init_pos=1, end_pos=CHR_SIZES,
    #       default_value=ParserOpt.DEFAULT_VALUE)