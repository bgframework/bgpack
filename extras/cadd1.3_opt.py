import csv
import gzip
import logging

from bgpack.parse.csv import CSVGrouped, ParserError
from bgpack.implementation.parser import optimize


class CADD(CSVGrouped):

    SCORE_ALT = {'T': 'ACG', 'A': 'CGT', 'C': 'AGT', 'G': 'ACT'}

    @staticmethod
    def parse(line):
        pos = int(line['Pos'])
        chrom = line.get('Chrom', line['#Chrom'])
        ref = line['Ref']
        alt = line['Alt']
        score = float(line['PHRED'])
        return chrom, pos, ref, alt, score

    @staticmethod
    def read(reader, check=False):
        v1 = CADD.parse(next(reader))
        # Skip M and R labeled nucleotides
        while v1[2] in ['M', 'R']:
            v1 = CADD.parse(next(reader))
        v2 = CADD.parse(next(reader))
        v3 = CADD.parse(next(reader))

        if check and not (v1[0] == v2[0] == v3[0] and v1[1] == v2[1] == v3[1] and v1[2] == v2[2] == v3[2]):
            print('Error in lines {}--{}--{}'.format(v1,v2,v3))
            raise ParserError('These lines are not consistent {}, {}, {}'.format(v1, v2, v3))
        position = v1[1]
        chromosome = v1[0]
        ref = v1[2]

        scores = [None] * 4
        for i, a in enumerate('ACGT'):
            for v in [v1, v2, v3]:
                if v[3] == a:
                    scores[i] = v[4]
                    break

        return chromosome, position, tuple(scores)

    def __init__(self, file):

        logging.getLogger(__name__).debug('Reading CADD scores from %s' % file)

        self.pos = 0

        self.file = file

    def __iter__(self):
        """

        Yield:
            tuple: key and the associated parser for that key

        """

        with gzip.open(self.file, 'rt') as fd:
            fd.readline()
            reader = iter(csv.DictReader(fd, delimiter='\t'))

            while True:
                try:
                    chr, __, vals = CADD.read(reader, check=True)
                    # if chr != 'chr22':
                    #     continue
                    yield (self.pos,) + vals
                    self.pos += 1
                except StopIteration:
                    break


if __name__ == '__main__':
    parser = CADD('/workspace/datasets/CADD/v1.3/CADD_v1.3/prescored/whole_genome_SNVs.tsv.gz')
    optimize(parser)
