import csv
import gzip
import logging

from bgpack.parse.utils import AbstractParserGrouped, ParserError

sizes=\
"""chr1	249250621
chr2	243199373
chr3	198022430
chr4	191154276
chr5	180915260
chr6	171115067
chr7	159138663
chrX	155270560
chr8	146364022
chr9	141213431
chr10	135534747
chr11	135006516
chr12	133851895
chr13	115169878
chr14	107349540
chr15	102531392
chr16	90354753
chr17	81195210
chr18	78077248
chr20	63025520
chrY	59373566
chr19	59128983
chr22	51304566
chr21	48129895"""
CHR_SIZES = {s.split()[0].replace('chr', ''):int(s.split()[1]) for s in sizes.split('\n') }


class CADD(AbstractParserGrouped):

    @staticmethod
    def parse(line):
        pos = int(line['Pos'])
        chrom = line.get('Chrom', line['#Chrom'])
        ref = line['Ref']
        alt = line['Alt']
        score = float(line['PHRED'])
        return chrom, pos, ref, alt, score

    @staticmethod
    def read(reader, check=False):
        v1 = CADD.parse(next(reader))
        # Skip Unknown chroms
        while v1[0] not in CHR_SIZES:
            v1 = CADD.parse(next(reader))
        # Skip M and R labeled nucleotides
        while v1[2] in ['M', 'R']:
            v1 = CADD.parse(next(reader))
        v2 = CADD.parse(next(reader))
        v3 = CADD.parse(next(reader))

        if check and not (v1[0] == v2[0] == v3[0] and v1[1] == v2[1] == v3[1] and v1[2] == v2[2] == v3[2]):
            print('Error in lines {}--{}--{}'.format(v1,v2,v3))
            raise ParserError('These lines are not consistent {}, {}, {}'.format(v1, v2, v3))
        position = v1[1]
        chromosome = v1[0]
        ref = v1[2]

        scores = [None] * 4
        for i, a in enumerate('ACGT'):
            for v in [v1, v2, v3]:
                if v[3] == a:
                    scores[i] = v[4]
                    break

        return chromosome, position, tuple(scores)

    def __init__(self, file):

        logging.getLogger(__name__).debug('Reading CADD scores from %s' % file)

        self.file = file

    def get_parsers(self):
        """

        Yield:
            tuple: key and the associated parser for that key

        """
        current_key = None
        first_pos = None
        first_val = None

        keep_going = True

        with gzip.open(self.file, 'rt') as fd:
            fd.readline()
            reader = iter(csv.DictReader(fd, delimiter='\t'))

            while keep_going:

                # Read first key and first value skipping comments
                if current_key is None:
                    current_key, first_pos, first_val = CADD.read(reader)

                # Create a parser
                def parser(check=True):
                    nonlocal current_key, first_pos, first_val, keep_going

                    yield first_pos, first_val

                    while True:
                        try:
                            this_key, pos, data = CADD.read(reader, check)
                        except StopIteration:
                            keep_going = False
                            break
                        except KeyError:
                            logging.debug('Skipping {}-{}-{}'.format(this_key, pos, data))
                            continue  # skip lines with unkown ref
                        if this_key != current_key:
                            current_key = this_key
                            first_pos = pos
                            first_val = data
                            break
                        else:
                            yield pos, data

                logging.getLogger(__name__).debug('Parser for key: %s' % current_key)

                if current_key not in CHR_SIZES.keys():
                    for _ in parser(): pass

                yield current_key, parser


if __name__ == '__main__':
    from bgpack import pack
    parser = CADD('/workspace/datasets/CADD/v1.3/CADD_v1.3/prescored/whole_genome_SNVs.tsv.gz')
    pack.create('cadd1.3', parser, init_pos=1, end_pos=CHR_SIZES, allow_multifile=True, optimize_for='size')
