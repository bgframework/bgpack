import functools
import random

import original


from bgpack.pack import create
from bgpack.unpack import load


caddpack_folder = '/home/ireyes/.bgdata/genomicscores/caddpack/1.0-20170217'
caddpack_reader = original.PackScoresReader(caddpack_folder)
CHROMOSOMES = list(caddpack_reader.metadata['positions'].keys())
POSITIONS = {chr: (v[2], v[3]) for chr, v in caddpack_reader.metadata['positions'].items()}


def caddpackparser(chr, start, stop):
    with caddpack_reader as r:
        it = iter(r.get(chr, start, start+7))
        while True:
            try:
                score1, ref1, alt1, pos1 = next(it)
                score2, ref2, alt2, pos2 = next(it)
                score3, ref3, alt3, pos3 = next(it)
                assert pos1 == pos2 == pos3
                yield pos1, ref1, score1, score2, score3
            except StopIteration:
                break


def pack():
    parsers = {}
    for chr, vals in caddpack_reader.metadata['positions'].items():
        x, y, start, stop = vals

        parsers[chr] = functools.partial(caddpackparser, chr=chr, start=start, stop=stop)

    # for i in parsers['6']():
    #     print(i)
    #     break

    create('cadd1.0', parsers, allow_multifile=False)




class Reader:

    SCORE_ALT = {'T': 'ACG', 'A': 'CGT', 'C': 'AGT', 'G': 'ACT'}

    def __init__(self):
        self.reader = load('cadd1.0')
        self.r = None

    def __enter__(self):
        self.r = self.reader.__enter__()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self.reader.__exit__(exc_type, exc_val, exc_tb)

    def get(self, chr, start, stop):
        for pos, ref, v1, v2, v3 in self.r.get(chr, start, stop):

            if ref is not None:
                yield v1, ref, Reader.SCORE_ALT[ref][0], pos
                yield v2, ref, Reader.SCORE_ALT[ref][1], pos
                yield v3, ref, Reader.SCORE_ALT[ref][2], pos

reader = Reader()

def compare(n=1000):
    for _ in range(n):
        chr = random.choice(CHROMOSOMES)
        start, stop = POSITIONS[chr]
        pos = random.randint(start, start+7)
        with caddpack_reader as caddpack:
            with reader as r:
                for i, j in zip(caddpack.get(chr, pos, pos), r.get(chr, pos, pos)):
                    assert i == j


if __name__ == '__main__':
    # pack()
    compare()
