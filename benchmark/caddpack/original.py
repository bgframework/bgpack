import gzip
import json
import os

import struct

import mmap


class ReaderError(Exception):

    def __init__(self, msg):
        self.message = msg


class PackScoresReader:

    STRUCT_SIZE = 6
    SCORE_ALT = {'T': 'ACG', 'A': 'CGT', 'C': 'AGT', 'G': 'ACT'}
    BIT_TO_REF = {
        (0, 0, 0): '?',
        (0, 0, 1): 'T',
        (0, 1, 0): 'A',
        (0, 1, 1): 'C',
        (1, 0, 0): 'G'
    }

    def __init__(self, folder):

        self.file_path = os.path.join(folder, 'whole_genome_SNVs.fml')

        with gzip.open('{}.idx'.format(self.file_path), 'rt') as fd:
            index = json.load(fd)

        self.scores = index['scores']
        self.metadata = index['metadata']
        self.fd = None

    def unpack(self, block):
        values = struct.unpack('3H', block)
        bits = tuple([v % 2 for v in values])
        values = [v // 2 for v in values]
        ref = PackScoresReader.BIT_TO_REF[bits]
        return ref, values

    def __enter__(self):
        self.fd = open(self.file_path, 'rb')
        self.mm = mmap.mmap(self.fd.fileno(), 0, access=mmap.ACCESS_READ)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.mm.close()
        self.fd.close()

    def get(self, chromosome, start, stop, *args, **kwargs):

        if chromosome not in self.metadata['positions']:
            raise ReaderError("Chromosome '{}' not found".format(chromosome))

        c_ini, c_end, c_start, c_stop = self.metadata['positions'][chromosome]

        q_ini = c_ini + (start - c_start)
        q_end = q_ini + (stop - start)

        # Check boundaries
        q_ini = c_ini if q_ini < c_ini else q_ini
        q_end = c_end if q_end > c_end else q_end
        if q_end < q_ini:
            raise ReaderError("Position out of boundaries. Chr:{} start:{} stop:{}".format(chromosome, start, stop))

        b_now = q_ini
        p_now = start

        self.mm.seek(b_now*PackScoresReader.STRUCT_SIZE)
        while b_now <= q_end:
            ref, values = self.unpack(self.mm.read(PackScoresReader.STRUCT_SIZE))

            if ref != '?':
                yield self.scores[values[0]], ref, PackScoresReader.SCORE_ALT[ref][0], p_now
                yield self.scores[values[1]], ref, PackScoresReader.SCORE_ALT[ref][1], p_now
                yield self.scores[values[2]], ref, PackScoresReader.SCORE_ALT[ref][2], p_now

            p_now += 1
            b_now += 1
