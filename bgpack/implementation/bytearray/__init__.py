"""
Bytearray is an implementation where values mapped to integers
and those are translated into bytes using the minimum bytes
required.
Thus, it takes more memory than the bitarray01 implementation but is faster when reading.
"""