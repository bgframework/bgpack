import tempfile
from os import path

import pytest

from bgpack.parse.csv import CSVGrouped, CSV
from bgpack import write, get_unpacker

this_dir = path.dirname(path.abspath(__file__))
DATASET = path.join(this_dir, 'data.tsv')


def test1():
    parser = CSV(DATASET, 1, sep='\t', formats={0: 'int', 2: 'float'})

    positions = [1000, 1001, 2000, 2001]
    v1 = [1, 1, 2, 2]
    v2 = [12.43, 15.67, 10.61, 5.36]

    with tempfile.TemporaryDirectory() as tmpdirname:
        name = path.join(tmpdirname, 'test1')
        write(name, parser)

        reader = get_unpacker(name)

        with reader as r:
            for i, p in enumerate(positions):
                for _, *val in r.get(p, p):
                    assert val == [v1[i], v2[i]]


@pytest.mark.parametrize("multifile", [True, False])
def test2(multifile):
    parser = CSVGrouped(DATASET, 0, 1, sep='\t', formats={0: 'int', 2: 'float'})

    positions = {1: [1000, 1001], 2: [2000, 2001]}
    v = {1: [12.43, 15.67], 2: [10.61, 5.36]}

    with tempfile.TemporaryDirectory() as tmpdirname:
        name = path.join(tmpdirname, 'test2')
        write(name, parser, allow_multifile=multifile)

        reader = get_unpacker(name)

        with reader as r:
            for key in [1, 2]:
                for _, val in r.get(key):
                    assert val in v[key]


if __name__ == '__main__':
    test1()
    for v in [True, False]:
        test2(v)
