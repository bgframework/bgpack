import shutil

from bgpack.implementation.parser import optimize


def test1():
    # no optimization
    # Single value
    def parser():
        for i in range(7):
            yield i, i
    rows, tmp, default, bitarary, bytearray_ = optimize(parser)
    assert rows == 7
    assert bitarary is None
    assert bytearray_ is None
    shutil.rmtree(tmp)


def test2():
    # optimization
    # 3 bits per value -> 6. 3 with tuple
    def parser():
        for i in range(7):
            yield i, i, i

    rows, tmp, default, bitarary, bytearray_ = optimize(parser)
    assert rows == 7
    assert bitarary is not None
    assert bytearray_ is not None
    shutil.rmtree(tmp)


def test3():
    # optimization
    # 1 per value -> 2. 1 with tuple
    def parser():
        for i in range(4):
            yield i, i%2, i%2

    rows, tmp, default, bitarary, bytearray_ = optimize(parser)
    assert rows == 4
    assert bitarary is not None
    assert bytearray_ is not None
    shutil.rmtree(tmp)


def test4():
    # no optimization for bitarray but yes for bytearray
    # 1 per value -> 2. 2 with tuple
    def parser():
        yield 0, 0, 0
        yield 1, 1, 1
        yield 2, 0, 1
        yield 3, 1, 0

    optimize(parser)

    rows, tmp, default, bitarary, bytearray_ = optimize(parser)
    assert rows == 4
    assert bitarary is None
    assert bytearray_ is not None
    shutil.rmtree(tmp)


if __name__ == '__main__':
    test1()
    test2()
    test3()
    test4()
