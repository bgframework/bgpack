from os import path

from bgpack.parse.csv import CSV, CSVGrouped
from bgpack.parse.utils import data_size

this_dir = path.dirname(path.abspath(__file__))
DATASET = path.join(this_dir, 'data.tsv')


def test1():
    """Normal parser"""
    parser = CSV(DATASET, position=1, sep='\t')
    assert data_size(parser) == 2
    positions = [1000, 1001, 2000, 2001]
    v1 = ['1','1','2','2']
    v2 = ['12.43', '15.67', '10.61', '5.36']
    for i, v in enumerate(parser()):
        assert v[0] == positions[i]
        assert v[1] == v1[i]
        assert v[2] == v2[i]


def test2():
    """Normal parser with gziped file"""
    parser = CSV(DATASET+'.gz', position=1, sep='\t')
    assert data_size(parser) == 2
    positions = [1000, 1001, 2000, 2001]
    v1 = ['1','1','2','2']
    v2 = ['12.43', '15.67', '10.61', '5.36']
    for i, v in enumerate(parser()):
        assert v[0] == positions[i]
        assert v[1] == v1[i]
        assert v[2] == v2[i]


def test3():
    """Normal parser with formats"""
    parser = CSV(DATASET, position=1, sep='\t', formats={0: 'int', 2: 'float'})
    assert data_size(parser) == 2
    positions = [1000, 1001, 2000, 2001]
    v1 = [1, 1, 2, 2]
    v2 = [12.43, 15.67, 10.61, 5.36]
    for i, v in enumerate(parser()):
        assert v[0] == positions[i]
        assert v[1] == v1[i]
        assert v[2] == v2[i]


def test4():
    """Normal parser filtering columns"""
    parser = CSV(DATASET, position=1, sep='\t', columns=[0], formats={0: 'int'})
    assert data_size(parser) == 1
    positions = [1000, 1001, 2000, 2001]
    v1 = [1,1,2,2]
    for i, v in enumerate(parser()):
        assert v[0] == positions[i]
        assert v[1] == v1[i]


def test5():
    """Normal parser with formats (reducing float decimals)"""
    parser = CSV(DATASET, position=1, sep='\t', formats={0: 'int', 2: 'f1'})
    assert data_size(parser) == 2
    positions = [1000, 1001, 2000, 2001]
    v1 = [1, 1, 2, 2]
    v2 = [12.4, 15.7, 10.6, 5.4]
    for i, v in enumerate(parser()):
        assert v[0] == positions[i]
        assert v[1] == v1[i]
        assert v[2] == v2[i]


def test10():
    """Grouped parser"""
    parser = CSVGrouped(DATASET, 0, position=1, sep='\t')
    assert data_size(parser) == 1
    positions = [1000, 1001, 2000, 2001]
    values = ['12.43', '15.67', '10.61', '5.36']
    names = ['1', '2']
    for i, v_i in enumerate(parser.get_parsers()):
        name, p = v_i
        assert name == names[i]
        for j, v_j in enumerate(p()):
            pos, v = v_j
            assert pos == positions[i*2+j]
            assert v == values[i*2+j]  # 2 because is the number of items in each group


def test11():
    """Grouped parser gziped"""
    parser = CSVGrouped(DATASET+'.gz', 0, 1, sep='\t')
    assert data_size(parser) == 1
    positions = [1000, 1001, 2000, 2001]
    values = ['12.43', '15.67', '10.61', '5.36']
    names = ['1', '2']
    for i, v_i in enumerate(parser.get_parsers()):
        name, p = v_i
        assert name == names[i]
        for j, v_j in enumerate(p()):
            pos, v = v_j
            assert pos == positions[i*2+j]
            assert v == values[i*2+j]  # 2 because is the number of items in each group


def test12():
    """Grouped parser with formats"""
    parser = CSVGrouped(DATASET, 0, 1, sep='\t', formats={0: 'int', 2: 'float'})
    assert data_size(parser) == 1
    positions = [1000, 1001, 2000, 2001]
    values = [12.43, 15.67, 10.61, 5.36]
    names = [1, 2]
    for i, v_i in enumerate(parser.get_parsers()):
        name, p = v_i
        assert name == names[i]
        for j, v_j in enumerate(p()):
            pos, v = v_j
            assert pos == positions[i*2+j]
            assert v == values[i*2+j]  # 2 because is the number of items in each group


if __name__ == '__main__':
    test1()
    test2()
    test3()
    test4()
    test5()

    test10()
    test11()
    test12()
