import functools

from bgpack.implementation.bytearray.mapper import MapperSingleValue, Mapper, MapWriter, \
    MapWriterManager
from bgpack.map.utils import MapError


def int2bytes(value, nbytes):
    return int(value).to_bytes(nbytes, 'big', signed=False)


def test1():
    # Simple writer
    s = set('abcdefg')
    mapper = MapWriter(s)

    assert mapper.nbytes == 1

    l = list(s)
    assert mapper['a'] == l.index('a').to_bytes(mapper.nbytes, 'big', signed=True)
    assert mapper['c'] == l.index('c').to_bytes(mapper.nbytes, 'big', signed=True)
    assert mapper['g'] == l.index('g').to_bytes(mapper.nbytes, 'big', signed=True)

    try:
        mapper['w']
    except KeyError:
        assert True
    else:
        assert False


def test2():
    # Simple reader
    s = set('abcde')
    writer = MapWriter(s)

    mapper = writer.get_reader()

    l = list(s)

    assert mapper.nbits == 8

    pos2key = functools.partial(int2bytes, nbytes=mapper.nbytes)

    assert mapper[pos2key(0)] == l[0]
    assert mapper[pos2key(2)] == l[2]
    assert mapper[pos2key(4)] == l[4]

    try:
        mapper[pos2key(7)]
    except KeyError:
        assert True
    else:
        assert False


def test3():
    # Simple writer and reader
    s = set('abcdefg')
    mapper_w = MapWriter(s)
    mapper_r = mapper_w.get_reader()

    assert 'a' == mapper_r[mapper_w['a']]
    assert 'c' == mapper_r[mapper_w['c']]
    assert 'g' == mapper_r[mapper_w['g']]
    assert 'f' == mapper_r[mapper_w['f']]
    assert 'b' == mapper_r[mapper_w['b']]

    assert mapper_w.nbytes == mapper_r.nbytes


def test4():
    # Writer
    m = MapWriter(set([10, 11, 12, 13, 14, 15]))

    mapper = MapWriterManager([MapWriter(set('abc')), m, m])

    try:
        mapper[('a', 1, 10)]
    except KeyError:
        assert True
    else:
        assert False

    try:
        mapper['w']
    except KeyError:
        assert True
    else:
        assert False

    assert mapper['b', 10, 10] != mapper['a', 10, 10]


def test5():
    # Writer and reader
    m = MapWriter(set([10, 11, 12, 13, 14, 15]))
    mapper_w = MapWriterManager([MapWriter('abc'), m, m])
    mapper_r = mapper_w.get_reader()

    # Seen values
    assert ('a', 10, 10) == mapper_r[mapper_w['a', 10, 10]]
    assert ('c', 14, 11) == mapper_r[mapper_w['c', 14, 11]]
    assert ('b', 15, 10) == mapper_r[mapper_w['b', 15, 10]]
    assert ('a', 13, 12) == mapper_r[mapper_w['a', 13, 12]]


def test6():
    # Constructor 1 value
    constructor = Mapper()
    constructor.add('a')
    constructor.add('b')
    constructor.add('b')
    constructor.add('b')
    constructor.add('c')
    constructor.add('c')
    constructor.add('c')
    constructor.add('c')
    constructor.add('c')
    constructor.add('d')
    constructor.add('e')
    constructor.add('f')
    constructor.add('g')

    writer, _ = constructor.get()
    reader = writer.get_reader()

    assert isinstance(writer, MapWriter)

    assert 'a' == reader[writer['a']]
    assert 'c' == reader[writer['c']]
    assert 'f' == reader[writer['f']]

    try:
        writer['w']
    except KeyError:
        assert True
    else:
        assert False

    try:
        reader[b'\xFF']
    except KeyError:
        assert True
    else:
        assert False


def test7():
    # Constructor multiple values
    constructor = Mapper()
    constructor.add('a', 10, 10)
    constructor.add('b', 11, 11)
    constructor.add('b', 10, 10)
    constructor.add('b', 13, 13)
    constructor.add('c', 14, 14)
    constructor.add('c', 15, 15)
    constructor.add('c', 10, 10)
    constructor.add('c', 13, 14)
    constructor.add('c', 10, 15)
    constructor.add('d', 13, 14)
    constructor.add('e', 14, 14)
    constructor.add('f', 10, 11)
    constructor.add('g', 15, 10)

    writer, _ = constructor.get()
    assert len(writer.maps) == 3
    assert len(set(map(id, writer.maps))) == 1  # all maps are the same
    assert isinstance(writer, MapWriterManager)

    try:
        writer['w']
    except KeyError:
        assert True
    else:
        assert False

    reader = writer.get_reader()

    try:
        reader[b'\xFF\xFF\xFF']
    except KeyError:
        assert True
    else:
        assert False

    assert ('a', 10, 10) == reader[writer['a', 10, 10]]
    assert ('b', 13, 13) == reader[writer['b', 13, 13]]
    assert ('c', 10, 10) == reader[writer['c', 10, 10]]
    assert ('c', 10, 10) == reader[writer['c', 10, 10]]
    assert ('c', 10, 10) == reader[writer['c', 10, 10]]
    assert ('c', 10, 10) == reader[writer['c', 10, 10]]
    assert ('c', 10, 10) == reader[writer['c', 10, 10]]
    assert ('c', 10, 10) == reader[writer['c', 10, 10]]

    # Unseen values might also be possible if they fit within the sets
    assert ('g', 10, 10) == reader[writer['g', 10, 10]]


def test8():
    # Constructor using a combination of values and tuples
    constructor = Mapper()
    constructor.add(('a', 10), 10)
    constructor.add(('b', 11), 11)
    constructor.add(('c', 12), 12)

    writer, _ = constructor.get()
    assert len(writer.maps) == 2
    assert isinstance(writer, MapWriterManager)

    try:
        writer['a', 10, 10]  # Now we are passing a tuple and a value not 3 values
    except KeyError:
        assert True
    else:
        assert False

    try:
        writer[('c', 10), 12]  # tuple ('c', 10) has never been seen
    except KeyError:
        assert True
    else:
        assert False

    reader = writer.get_reader()

    try:
        reader[b'\xFF\xFF']
    except KeyError:
        assert True
    else:
        assert False

    assert (('a', 10), 10) == reader[writer[('a', 10), 10]]
    assert (('b', 11), 11) == reader[writer[('b', 11), 11]]
    assert (('c', 12), 12) == reader[writer[('c', 12), 12]]

    # Unseen values might also be possible if they fit within the sets
    assert (('c', 12), 10) == reader[writer[('c', 12), 10]]


def test9():
    # Constructor default value
    constructor = Mapper()
    try:
        constructor.set_default_value(None)
    except MapError:
        assert True
    else:
        assert False

    constructor = Mapper()
    constructor.add(('a', 10), 10)
    constructor.set_default_value(None)
    writer, dv = constructor.get()
    assert writer[None, None] == writer[dv]
    reader = writer.get_reader()
    assert dv == reader[writer[dv]]

    constructor = Mapper()
    constructor.add(10)
    constructor.set_default_value(None)
    writer, dv = constructor.get()
    assert writer[None] == writer[dv]
    reader = writer.get_reader()
    assert None is reader[writer[dv]]


def test10():
    # Constructor 1 value using optimized mapper
    constructor = MapperSingleValue()
    constructor.add('a')
    constructor.add('b')
    constructor.add('b')
    constructor.add('b')
    constructor.add('c')
    constructor.add('c')
    constructor.add('c')
    constructor.add('c')
    constructor.add('c')
    constructor.add('d')
    constructor.add('e')
    constructor.add('f')
    constructor.add('g')

    writer, _ = constructor.get()
    reader = writer.get_reader()

    assert isinstance(writer, MapWriter)

    assert 'a' == reader[writer['a']]
    assert 'c' == reader[writer['c']]
    assert 'f' == reader[writer['f']]

    try:
        writer['w']
    except KeyError:
        assert True
    else:
        assert False

    try:
        reader['\xFF']
    except KeyError:
        assert True
    else:
        assert False


if __name__ == '__main__':
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
    test7()
    test8()
    test9()
    test10()
