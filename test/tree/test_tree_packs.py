import tempfile

from os import path

from bgpack.parse.csv import CSVGrouped, CSV
from bgpack.tree import write, get_unpacker

this_dir = path.dirname(path.abspath(__file__))
DATASET = path.join(this_dir, 'data.tsv')


def test_single_value():
    parser = CSV(DATASET, 1, sep='\t', columns=[2], formats={0: 'int', 2: 'float'})

    positions = [1000, 1001, 2000, 2001, 3000, 3001]
    v1 = [1, 1, 2, 2, 2, 2]
    v2 = [12.43, 15.67, 10.61, 5.36, 7.29, 18.63]

    with tempfile.TemporaryDirectory() as tmpdirname:
        name = path.join(tmpdirname, 'test_single_value')
        write(name, parser)

        reader = get_unpacker(name)

        with reader as r:
            for i, v in enumerate(r.get(1000, 2001)):
                assert v == (positions[i], v2[i])


def test_multi_value():
    parser = CSV(DATASET, 1, sep='\t', formats={0: 'int', 2: 'float'})

    positions = [1000, 1001, 2000, 2001, 3000, 3001]
    v1 = [1, 1, 2, 2, 2, 2]
    v2 = [12.43, 15.67, 10.61, 5.36, 7.29, 18.63]

    with tempfile.TemporaryDirectory() as tmpdirname:
        name = path.join(tmpdirname, 'test_multi_value')
        write(name, parser)

        reader = get_unpacker(name)

        with reader as r:
            for i, v in enumerate(r.get(1000, 2001)):
                assert v == (positions[i], v1[i], v2[i])


def test_grouped():
    parser = CSVGrouped(DATASET, 0, 1, sep='\t', formats={0: 'int', 2: 'float'})

    positions = {1: [1000, 1001], 2: [2000, 2001, 3000, 3001]}
    values = {1: [12.43, 15.67], 2: [10.61, 5.36, 7.29, 18.63]}

    with tempfile.TemporaryDirectory() as tmpdirname:
        name = path.join(tmpdirname, 'test_grouped')
        write(name, parser)

        reader = get_unpacker(name)

        with reader as r:
            for key in [1, 2]:
                pos = positions[key]
                val = values[key]
                for i, v in enumerate(r.get(key)):
                    assert v == (pos[i], val[i])


def test_limits():
    parser = CSV(DATASET, 1, sep='\t', columns=[2], formats={0: 'int', 2: 'float'})

    positions = [1000, 1001, 2000, 2001, 3000, 3001]
    v2 = [12.43, 15.67, 10.61, 5.36, 7.29, 18.63]

    with tempfile.TemporaryDirectory() as tmpdirname:
        name = path.join(tmpdirname, 'test_limits')
        write(name, parser, init_pos=998, end_pos=4000)

        reader = get_unpacker(name)

        with reader as r:
            for i, v in enumerate(r.get(999, 3958)):
                assert v == (positions[i], v2[i])


if __name__ == '__main__':
    test_single_value()
    test_multi_value()
    test_grouped()
    test_limits()
